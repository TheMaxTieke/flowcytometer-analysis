#!/usr/bin/python

import segdata
import numpy as np
import matplotlib.pyplot as plt
import os

if __name__ == '__main__':

    # THIS IS A FIX AS THERE IS NO BATCH TIME IN MAX'S EXPERIMENTS
    segdata.data._DELAY=0.

    
    prefix=r'E:\Backup\Uni\Master\Masterthesis-Data'
    suffix=(['Accelerostat I/MT_JA_Serevisiae_glc3_accelerostat_230721',
             'Accelerostat II/MT_JA_Serevisiae_glc3_accelerostat_090921',
             'Accelerostat III/MT_JASereviciae_glc3_accelerostat_161021',
             'Deaccelerostat I/MT_JA_Serevisiae_glc3_deaccelerostat_040821',
             'Deaccelerostat II/MT_JA_Serevisiae_glc3_deaccelerostat_071021',
             'Deaccelerostat III/111121'])
             
    
    paths=[]

    for s in suffix:
        paths.append(os.path.join(prefix, s))


    for i,name in enumerate(paths):

        d=segdata.data.Data(name)

        # remove <=0, inf, nan
        # threshold means at least 90% of data needs to be useful
        # selecting all channels (channels=np.arange(6))
        # makes it delete all datasets so I'll ignore that
        # d.sanitize(threshold=0.9, channels=np.arange(6))
        d.sanitize(threshold=0.9)

        # threshold on FSCH = 6
        d.gate(6,min=10**5.5, threshold=0.9)

        # remove doublets considered as 0.5 < FSCA/FSCH < 1.75
        # doublet_threshold means removed if more than (1-0.975)*100 % of points
        # are doublets
        d.rm_doublets(min=0.5, max=1.75, threshold=0.9, doublet_threshold=0.975)

        # statistics on the whole population
        s0=d.statistics() # not implemented yet
        # s0 is pandas object

        # find threshold to segregate population (see JA's code)
        # t= d.find_threshold() # not implemented yet
        t=10.**3.5

        # make two populations, "positive" will be the one above t
        # 2 is the property, here FL1-A (see data structure)
        d.add_seg_rule(2, "positive", min=t)

        # statistics on both parts of the population
        s1=d.statistics(population="positive", positive=True)
        s2=d.statistics(population="positive", positive=False)

        # get proportion of positive over time and add to s0
        s0["positive proportion [percent]"]=100*s1["Number of samples"]/s0["Number of samples"]

        # remove annoying columns
        for col in s0.columns:
            if (
                        ("FL2" in col)
                    or
                        ("FL3" in col)
                    or
                        ("FL4" in col)
                    ):
                del s0[col]
                del s1[col]
                del s2[col]

        # save files
        s0.to_csv("full_pop"+os.path.basename(name)+".csv", sep=";")
        s1.to_csv("pos_pop"+os.path.basename(name)+".csv", sep=";")
        s2.to_csv("neg_pop"+os.path.basename(name)+".csv", sep=";")

        fig, ax = plt.subplots(figsize=(7.5,4))
        p=segdata.Plot(d)
        p.time_scatter(ax, d, index=6, y_label='FSC-H', y_log_lim=[5.4,6.7], log_scale=False)
        ax.vlines(14, 0, 10**7, colors='r')
        # for Accelerostat 
        if suffix[i][0]=='A':
            ax.vlines(74, 0, 10**7, colors='r')
        else:
        #for Deaccelerostat
            ax.vlines(27, 0, 10**7, colors='r')

        #plt.show()
        plt.savefig('time_scatter_linear'+os.path.basename(name)+'.png', bbox_inches='tight',  dpi = (200))

#!/usr/bin/python

import segdata
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import matplotlib.colors
import os
import pandas as pd


if __name__ == '__main__':

    # THIS IS A FIX AS THERE IS NO BATCH TIME IN MAX'S EXPERIMENTS
    segdata.data._DELAY=0.

    # prefix='/home/vogu/00-doctorat/extra code/dummy_experiment' # testing
    # suffix=(['MT_JA_Serevisiae_glc3_accelerostat_090921']) # testing


    prefix=r'H:\Backup\Uni\Master\Masterthesis-Data'
    suffix=(['Accelerostat I/MT_JA_Serevisiae_glc3_accelerostat_230721',
             'Accelerostat II/MT_JA_Serevisiae_glc3_accelerostat_090921',
             'Accelerostat III/MT_JASereviciae_glc3_accelerostat_161021',
             'Deaccelerostat I/MT_JA_Serevisiae_glc3_deaccelerostat_040821',
             'Deaccelerostat II/MT_JA_Serevisiae_glc3_deaccelerostat_071021',
             'Deaccelerostat III/111121'])

    paths=[]

    # dilution rate interval on which to do the regression, per experiment:
    reg_interval=np.array([[0.07,0.27], # A I
                          [0.07,0.27], # A II
                          [0.07,0.25], # A III
                          [0.07,0.248], # D I
                          [0.07,0.248], # D II
                          [0.07,0.225]]) # D III
    # percentile of interest for the regression
    percent=99.9

    for s in suffix:
        paths.append(os.path.join(prefix, s))

    dilutions=pd.read_excel('dilution_rates.xlsx')
    dilutions=dilutions.to_numpy()
    dilutions=np.delete(dilutions, 0, 0)

    for i, name in enumerate(paths):

        fig, ax=plt.subplots(figsize=(7.5,4))

        timing=dilutions[:,2*i]
        rates=dilutions[:,2*i+1]
        to_del=np.isnan(timing)
        timing=np.delete(timing, to_del)
        rates=np.delete(rates, to_del)

        d=segdata.Data(name)

        # remove <=0, inf, nan
        # threshold means at least 90% of data needs to be useful
        # selecting all channels (channels=np.arange(6))
        # makes it delete all datasets so I'll ignore that
        # d.sanitize(threshold=0.9, channels=np.arange(6))

        d.sanitize(threshold=0.9)

        # threshold on FSCH = 6
        d.gate(6,min=10**5.5, threshold=0.9)

        # remove doublets considered as 0.5 < FSCA/FSCH < 1.75
        # doublet_threshold means removed if more than (1-0.975)*100 % of points
        # are doublets
        d.rm_doublets(min=0.5, max=1.75, threshold=0.9, doublet_threshold=0.975)

        # x is the dilution rate
        x=[]
        # y is the measurement
        y=[]
        for j, rate in enumerate(rates[:-1]):
            t1=np.searchsorted(d.flow[:,0],timing[j])
            t2=np.searchsorted(d.flow[:,0],timing[j+1], side="right")
            for k in range(t1, t2):
                new_x=rate*np.ones_like(d.flow[k,5].cyto[:,2])
                new_y=d.flow[k,5].cyto[:,2]
                x.append(new_x)
                y.append(new_y)
        x=np.concatenate(x)
        y=np.concatenate(y)

        y_res=100

        logbins=np.logspace(0,6,y_res)
        rates=np.delete(rates, -1, 0)

        # for the excel file:
        d=np.zeros((rates.shape[0]+2, y_res-1))
        d[0,:]=logbins[:-1]
        d[1,:]=logbins[1:]

        # for the second excel file
        d_weighted=np.copy(d)

        weights=np.ones_like(x)
        sum_r=rates.shape[0]
        for j, rate in enumerate(rates):
            interest=(x==rate)
            current_weight=1/np.sum(interest)
            weights[interest]=current_weight

            # for the excel file:
            d[j+2,:]=np.histogram(y[interest], bins=logbins)[0]
            # for the second excel file
            d_weighted[j+2,:]=d[j+2,:]*current_weight

        x_bins=(rates[1:]+rates[:-1])/2
        x_bins=np.concatenate([[2*rates[0]-rates[1]], x_bins,
                               [2*rates[-1]-rates[-2]]])

        if suffix[i][0]=='D':
            x_bins=x_bins[::-1]

        c=cm.ScalarMappable(cmap='viridis')

        hb=ax.hist2d(x,y,
            bins=[x_bins.astype('float64'), logbins.astype('float64')],
            cmap=c.get_cmap(), weights=weights,
            norm=matplotlib.colors.LogNorm())

        ax.set_yscale('log')

        ##############################
        # Add a linear regression
        ##############################
        # Could have added that within the rest of the calculations,
        # but it's messy enough as it is
        percentiles=np.zeros_like(rates)
        # there should not be 0 in this data anyways
        for j, rate in enumerate(rates):
            # if in range for the regression
            if (rate >= reg_interval[i,0]) and (rate <= reg_interval[i,1]):
                interest=(x==rate) # select points of interest for current rate
                # if there is actually data for the rate of interest:
                if np.sum(interest)>0:
                    percentiles[j]=np.percentile(y[interest], percent)
                    # get the percentile
            # else it's zero and will be removed


        rates_2=rates[percentiles>0]
        percentiles=percentiles[percentiles>0]

        # plot experimental percentiles
        # comment out to erase
        #ax.plot(rates_2, percentiles, 'ro')

        coeff, r2, _, _, _=np.polyfit(rates_2, np.log(percentiles), 1, full=True)
        # log(percentile)=coeff[0] * rate + coeff[1]
        print(os.path.basename(name))
        print("log(percentile)=coeff[0] * rate + coeff[1]")
        print("coeff:")
        print(coeff)
        print("r^2 (I think, tell me if the values there don't make sense):")
        print(1-(r2/rates_2.shape[0])/np.var(np.log(percentiles)))

        # plot linear regression
        ax.plot(rates_2, np.exp(coeff[0] * rates_2 + coeff[1]) , 'r-')

        ##############################


        plt.savefig('dilutions-weighted-'+os.path.basename(name)+'.png', bbox_inches='tight',  dpi = (200))

        # for the excel file:
        rates=np.concatenate((["Bin left","Bin right"],rates))
        to_save=pd.DataFrame(d, index=rates, copy=False)
        to_save.to_csv("dilution_scatter"+os.path.basename(os.path.dirname(name))+".csv", sep=";")

        # for the second excel file
        to_save=pd.DataFrame(d_weighted, index=rates, copy=False)
        to_save.to_csv("dilution_scatter_weighted_"+
                        os.path.basename(os.path.dirname(name))+".csv", sep=";")

#!/usr/bin/python

import segdata
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import matplotlib.colors
import os
import pandas as pd


if __name__ == '__main__':

    # THIS IS A FIX AS THERE IS NO BATCH TIME IN MAX'S EXPERIMENTS
    segdata.data._DELAY=0.

    # prefix='/home/vogu/00-doctorat/extra code/dummy_experiment' # testing
    # suffix=(['MT_JA_Serevisiae_glc3_accelerostat_090921']) # testing


    prefix='/run/media/vogu/Seagate Expansion Drive/Backup/Uni/Master/Masterthesis-Data'
    suffix=(['Accelerostat I/MT_JA_Serevisiae_glc3_accelerostat_230721',
             'Accelerostat II/MT_JA_Serevisiae_glc3_accelerostat_090921',
             'Accelerostat III/MT_JASereviciae_glc3_accelerostat_161021',
             'Deaccelerostat I/MT_JA_Serevisiae_glc3_deaccelerostat_040821',
             'Deaccelerostat II/MT_JA_Serevisiae_glc3_deaccelerostat_071021',
             'Deaccelerostat III/111121'])

    paths=[]

    for s in suffix:
        paths.append(os.path.join(prefix, s))

    dilutions=pd.read_excel('qs.xlsx')
    dilutions=dilutions.to_numpy()

    for i, name in enumerate(paths):

        fig, ax=plt.subplots(figsize=(7.5,4))

        timing=dilutions[:,2*i]
        qs=dilutions[:,2*i+1]
        to_del=np.isnan(qs)
        timing=np.delete(timing, to_del)
        qs=np.delete(qs, to_del)

        d=segdata.Data(name)

        # remove <=0, inf, nan
        # threshold means at least 90% of data needs to be useful
        # selecting all channels (channels=np.arange(6))
        # makes it delete all datasets so I'll ignore that
        # d.sanitize(threshold=0.9, channels=np.arange(6))

        d.sanitize(threshold=0.9)

        # threshold on FSCH = 6
        d.gate(6,min=10**5.5, threshold=0.9)

        # remove doublets considered as 0.5 < FSCA/FSCH < 1.75
        # doublet_threshold means removed if more than (1-0.975)*100 % of points
        # are doublets
        d.rm_doublets(min=0.5, max=1.75, threshold=0.9, doublet_threshold=0.975)

        x=[]
        y=[]
        for j, q in enumerate(qs):
            t=np.searchsorted(d.flow[:,0],timing[j])
            for k in range(t-2, t+1):
                try:
                    new_x=q*np.ones_like(d.flow[k,5].cyto[:,2])
                    new_y=d.flow[k,5].cyto[:,2]
                    x.append(new_x)
                    y.append(new_y)
                except IndexError:
                    print("index error")
                    print(t)
        x=np.concatenate(x)
        y=np.concatenate(y)

        logbins=np.logspace(0,6,100)


        weights=np.ones_like(x)
        sum_r=qs.shape[0]
        for q in qs:
            interest=(x==q)
            weights[interest]=sum_r/np.sum(interest)

        qs=np.sort(qs)

        x_bins=(qs[1:]+qs[:-1])/2
        x_bins=np.concatenate([[2*qs[0]-qs[1]], x_bins,
                               [2*qs[-1]-qs[-2]]])


        c=cm.ScalarMappable(cmap='viridis')

        hb=ax.hist2d(x,y,
            bins=[x_bins.astype('float64'), logbins.astype('float64')],
            cmap=c.get_cmap(), weights=weights,
            norm=matplotlib.colors.LogNorm())
        ax.set_yscale('log')

        plt.savefig('qs-weighted-'+os.path.basename(name)+'.png', bbox_inches='tight',  dpi = (200))

#!/usr/bin/python

import segdata
import numpy as np
import matplotlib.pyplot as plt
import os

if __name__ == '__main__':

    # THIS IS A FIX AS THERE IS NO BATCH TIME IN MAX'S EXPERIMENTS
    segdata.data._DELAY=0.

    paths=(['/run/media/vogu/Seagate Expansion Drive/Backup/Uni/Master/Masterthesis-Data/Accelerostat I/MT_JA_Serevisiae_glc3_accelerostat_230721/',
    '/run/media/vogu/Seagate Expansion Drive/Backup/Uni/Master/Masterthesis-Data/Accelerostat II/MT_JA_Serevisiae_glc3_accelerostat_090921/',
    '/run/media/vogu/Seagate Expansion Drive/Backup/Uni/Master/Masterthesis-Data/Accelerostat III/MT_JASereviciae_glc3_accelerostat_161021/',
    '/run/media/vogu/Seagate Expansion Drive/Backup/Uni/Master/Masterthesis-Data/Deaccelerostat I/MT_JA_Serevisiae_glc3_deaccelerostat_040821/',
    '/run/media/vogu/Seagate Expansion Drive/Backup/Uni/Master/Masterthesis-Data/Deaccelerostat II/MT_JA_Serevisiae_glc3_deaccelerostat_071021/',
    '/run/media/vogu/Seagate Expansion Drive/Backup/Uni/Master/Masterthesis-Data/Deaccelerostat III/111121/'])

    for name in paths:
        d=segdata.data.Data(name)

        # remove <=0, inf, nan
        # threshold means at least 90% of data needs to be useful
        # selecting all channels (channels=np.arange(6))
        # makes it delete all datasets so I'll ignore that
        # d.sanitize(threshold=0.9, channels=np.arange(6))

        d.sanitize(threshold=0.9)

        # threshold on FSCH = 6
        d.gate(6,min=10**5.5, threshold=0.9)

        # remove doublets considered as 0.5 < FSCA/FSCH < 1.75
        # doublet_threshold means removed if more than (1-0.975)*100 % of points
        # are doublets
        d.rm_doublets(min=0.5, max=1.75, threshold=0.9, doublet_threshold=0.975)


        scatter_data=d.time_scatter_like()
        scatter_data.to_csv("time_scatter"+os.path.basename(os.path.dirname(name))+".csv", sep=";")
        print("time_scatter"+os.path.basename(os.path.dirname(name))+".csv saved")

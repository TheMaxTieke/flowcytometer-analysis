#!/usr/bin/python

import segdata
import numpy as np
import matplotlib.pyplot as plt
import os

if __name__ == '__main__':

    # THIS IS A FIX AS THERE IS NO BATCH TIME IN MAX'S EXPERIMENTS
    segdata.data._DELAY=0.



    prefix=r'E:\Backup\Uni\Master\Masterthesis-Data'
    suffix=(['Accelerostat I/MT_JA_Serevisiae_glc3_accelerostat_230721',
             'Accelerostat II/MT_JA_Serevisiae_glc3_accelerostat_090921',
             'Accelerostat III/MT_JASereviciae_glc3_accelerostat_161021',
             'Deaccelerostat I/MT_JA_Serevisiae_glc3_deaccelerostat_040821',
             'Deaccelerostat II/MT_JA_Serevisiae_glc3_deaccelerostat_071021',
             'Deaccelerostat III/111121'])


    paths=[]

    for s in suffix:
        paths.append(os.path.join(prefix, s))


    for name in paths:
        d=segdata.data.Data(name)

        # remove <=0, inf, nan
        # threshold means at least 90% of data needs to be useful
        # selecting all channels (channels=np.arange(6))
        # makes it delete all datasets so I'll ignore that
        # d.sanitize(threshold=0.9, channels=np.arange(6))

        d.sanitize(threshold=0.9)

        # threshold on FSCH = 6
        d.gate(6,min=10**5.5, threshold=0.9)

        # remove doublets considered as 0.5 < FSCA/FSCH < 1.75
        # doublet_threshold means removed if more than (1-0.975)*100 % of points
        # are doublets
        d.rm_doublets(min=0.5, max=1.75, threshold=0.9, doublet_threshold=0.975)


        scatter_data=d.time_scatter_like(index=6, y_log_lim=[5.4,6.7], log_scale=False)
        scatter_data.to_csv("FSC_scatter"+os.path.basename(os.path.dirname(name))+".csv", sep=";")
        print("time_scatter"+os.path.basename(os.path.dirname(name))+".csv saved")

#!/usr/bin/python

import segdata
import numpy as np
import matplotlib.pyplot as plt

if __name__ == '__main__':

    # THIS IS A FIX AS THERE IS NO BATCH TIME IN MAX'S EXPERIMENTS
    segdata.data._DELAY=0.

    # d=segdata.data.Data('/home/vogu/00-doctorat/extra code/dummy_experiment/MT_JA_Serevisiae_glc3_accelerostat_230721/')
    # d=segdata.data.Data('/run/media/vogu/Seagate Expansion Drive/Backup/Uni/Master/Masterthesis-Data/Deaccelerostat I/MT_JA_Serevisiae_glc3_deaccelerostat_040821/')
    d=segdata.data.Data('/home/vogu/00-doctorat/extra code/dummy_experiment/MT_JA_Serevisiae_glc3_accelerostat_090921/')
    # remove <=0, inf, nan
    # threshold means at least 90% of data needs to be useful
    # selecting all channels (channels=np.arange(6))
    # makes it delete all datasets so I'll ignore that
    # d.sanitize(threshold=0.9, channels=np.arange(6))
    d.sanitize(threshold=0.9)

    # threshold on FSCH = 6
    d.gate(6,min=10**5.5, threshold=0.9)

    # remove doublets considered as 0.5 < FSCA/FSCH < 1.75
    # doublet_threshold means removed if more than (1-0.975)*100 % of points
    # are doublets
    d.rm_doublets(min=0.5, max=1.75, threshold=0.9, doublet_threshold=0.975)

    fig, ax = plt.subplots(figsize=(7.5,4))
    p=segdata.Plot(d)
    p.time_scatter(ax, d, index=6, y_label='FSC-H', y_log_lim=[5.4,6.7], log_scale=False)
    ax.vlines(14, 0, 10**7, colors='r')
    ax.vlines(74, 0, 10**7, colors='r')

    # plt.show()
    plt.savefig('accelerostat1_time_scatter_FSC.png', bbox_inches='tight',  dpi = (200))
